import React, { useContext } from "react";

import { PokemonContext, addPokemon } from "../../context/pokemonContext";
import { getPokemonById } from "../../services/pokeApi";

import PokemonCard from "../PokemonCard";
import FetchButton from "../FetchButton";

import "./styles.scss";

function PokemonList() {
  const [state, dispatch] = useContext(PokemonContext);
  const { pokemonList } = state;



  function handleFetchPokemon(id) {
    async function fetchPokemon() {
      const pokemon = await getPokemonById(id);
      dispatch(addPokemon(pokemon));
    }

    const hasPokemon = pokemonList.find(({ id: pkId }) => pkId === id);
    if (!hasPokemon) {
      fetchPokemon();
    }
  }

  return (
    <>
      <FetchButton
        initialCounter={pokemonList.length}
        onFetch={handleFetchPokemon}
      />
      <ul className="PokemonList__main-list">
        {pokemonList.map(pokemon => (
          <PokemonCard key={pokemon.id} {...pokemon} />
        ))}
      </ul>
    </>
  );
}

export default PokemonList;
