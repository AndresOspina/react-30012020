import React, { useEffect, useContext } from "react";
import PropTypes from "prop-types";

import PokemonCard from "../PokemonCard";

import { getPokemonById as fetchPokemonById } from "../../services/pokeApi";
import {
  PokemonContext,
  addPokemon,
  getPokemonById
} from "../../context/pokemonContext";

function PokemonProfile({ match }) {
  const [state, dispatch] = useContext(PokemonContext);

  const { id } = match.params;
  const pokemon = getPokemonById(state, id);

  useEffect(() => {
    async function fetchPokemon() {
      const pokemon = await fetchPokemonById(id);
      dispatch(addPokemon(pokemon));
    }

    if (!pokemon) {
      fetchPokemon();
    }
  }, [id, pokemon, dispatch]);

  return pokemon ? <PokemonCard {...pokemon} /> : <h3>Loading pokemon...</h3>;
}

PokemonProfile.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string
    })
  }).isRequired
};

export default PokemonProfile;
