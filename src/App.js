import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./containers/Home";
import Pokemon from "./containers/Pokemon";
// import PokemonProfile from "./containers/PokemonProfile";
import Test from "./containers/Test";

import "./App.scss";

function App() {
  return (
      <Router>
        <Switch>
          <Route path="/pokemon" component={Pokemon} />
          <Route path="/test" component={Test} />
          <Route path="/" component={Home} />
        </Switch>
      </Router>
  );
}

export default App;
