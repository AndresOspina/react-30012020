import React from "react";
import { Route, Switch, Link } from "react-router-dom";

import PokemonList from "../../components/PokemonList";
import PokemonProfile from "../../components/PokemonProfile";

import { PokemonContextProvider } from "../../context/pokemonContext";

function Pokemon({ match }) {
  return (
    <div className="Pokemon">
      <h1>Welcome to Upgradex</h1>
      <div>
        <Link to={`${match.path}/list`}>To Pokemon list</Link>
      </div>
      <PokemonContextProvider>
        <Switch>
          <Route path={`${match.path}/list`} exact component={PokemonList} />
          <Route path={`${match.path}/:id`} exact component={PokemonProfile} />
        </Switch>
      </PokemonContextProvider>
    </div>
  );
}

export default Pokemon;
