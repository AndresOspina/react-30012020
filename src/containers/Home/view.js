import React from "react";
import { Link } from 'react-router-dom';

function Home() {
  return (
    <>
      <h1>Welcome to Upgradex</h1>
      <h3>Fetch all the pokemon you want</h3>
      <ul>
        <li>
          <Link to="/pokemon">Pokemon</Link>
        </li>
        <li>
          <Link to="/test">Test sandbox</Link>
        </li>
      </ul>
    </>
  );
}

export default Home;